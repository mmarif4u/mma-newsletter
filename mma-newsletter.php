<?php
/*
Plugin Name: MMA Newsletter
Plugin URI: http://anl4u.com
Description: A simple newsletter plug for getting users email and exporting the data into CSV format.
Version: 0.1
Author: Mian Muhammad Arif
Author URI: http://anl4u.com
License: GPL2
*/

global $wpdb, $mma_newsletter_plugin_name, $mma_newsletter_plugin_version, $mma_newsletter_db_version;
$mma_newsletter_plugin_name = 'MMA Newsletter';
$mma_newsletter_plugin_version = "0.1";
$mma_newsletter_db_version = '0.1';
//$plugin_page = 'mma_newsletter';

// Define constants
define("MMA_NEWSLETTER_PLUGIN_WEBURL", plugin_dir_url( __FILE__ ));
define("MMA_NEWSLETTER_PLUGIN_URL", plugin_dir_path( __FILE__ ));
define("MMA_NEWSLETTER_PLUGIN_TABLE", $wpdb->prefix. "mma_newsletter");

if(is_admin()){
	require_once 'mma-newsletter-admin.php';
}

// Initializes the plugin
function mma_newsletter_init () {
	global $wpdb, $wp_query;
	
}

// Enque scripts
function mma_newsletter_scripts_css () {
	wp_enqueue_style( 'mma_newsletter_css', plugins_url('/css/styles.css', __FILE__));
	wp_enqueue_script('mma_newsletter_js', plugins_url('/js/main.js', __FILE__), 'jquery', '', 'in_footer');
}
add_action("wp_enqueue_scripts", "mma_newsletter_scripts_css"); // Include all stylesheets and scripts

// Enque scripts for Admin
function mma_newsletter_admin_scripts_css () {
	wp_enqueue_style( 'mma_newsletter_jq_ui_css', plugins_url('/css/jquery-ui-1.9.2.custom.min.css', __FILE__));
	wp_enqueue_style( 'mma_newsletter_styles_admin_css', plugins_url('/css/styles-admin.css', __FILE__));
	wp_enqueue_script('mma_newsletter_jq_ui_js', plugins_url('/js/jquery-ui-1.9.2.custom.min.js', __FILE__), 'jquery', '', 'in_footer');
	wp_enqueue_script('mma_newsletter_main_admin_js', plugins_url('/js/main-admin.js', __FILE__), 'jquery', '', 'in_footer');
}
add_action('admin_init', 'mma_newsletter_admin_scripts_css');

// Enque ajax scripts
function mma_newsletter_ajax_scripts () {
	wp_enqueue_script('ajaxnewsletter', plugins_url('/js/ajaxnewsletter.js', __FILE__), array('jquery'), false, true );
	wp_localize_script('ajaxnewsletter', 'ajaxnewsletterajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )) );
}
add_action('wp_enqueue_scripts', mma_newsletter_ajax_scripts);

// Register the installation hook
register_activation_hook( __FILE__, 'mma_newsletter_install' );

// Installation function to check or create necessary vars and tables
function mma_newsletter_install() {
	global $wpdb, $mma_newsletter_db_version, $mma_newsletter_plugin_version;
	$tbl_name = MMA_NEWSLETTER_PLUGIN_TABLE;	
	$version = get_option("mma_newsletter_db_version");
	$plug_version = get_option("mma_newsletter_plugin_version");

	// Create table
	if($version != $mma_newsletter_db_version) {
		$sql = "CREATE TABLE $tbl_name (
	  		id mediumint(9) NOT NULL AUTO_INCREMENT,
	  		email varchar(150) NOT NULL,
	  		time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
	  		UNIQUE KEY id (id)
			);";

	    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	    dbDelta($sql);

	    update_option("mma_newsletter_db_version", $mma_newsletter_db_version);
	} else {
		add_option("mma_newsletter_db_version", $mma_newsletter_db_version);
	}

	// Check for plugin version
	if($plug_version != $mma_newsletter_plugin_version) {
		update_option("mma_newsletter_plugin_version", $mma_newsletter_plugin_version);
	} else {
		add_option("mma_newsletter_plugin_version", $mma_newsletter_plugin_version);
	}

}

// Hook the main function to wp_head
add_action("wp_head", "mma_newsletter_init");

// Template inclusion
function mma_newsletter_template () {
	require_once(MMA_NEWSLETTER_PLUGIN_URL."template.php");
}

// Function to save data through ajax call
function mma_newsletter_save_data () {
	global $wpdb;
	$tbl_name = MMA_NEWSLETTER_PLUGIN_TABLE;
	$email = $_POST['email'];
	$time = date('Y-m-d H:i:s');
	if(is_email($email)) { // Check if email is valid through WP is_email function, if yes save data.
		$wpdb->query( $wpdb->prepare (
			"INSERT INTO $tbl_name
			( email, time ) VALUES ( %s, %s )",  
			$email, 
			$time
		));
	} else {
		//nothing to do
	}
}

// Register the save data function hook
add_action( 'wp_ajax_nopriv_mma_newsletter_save_data', 'mma_newsletter_save_data' );
add_action( 'wp_ajax_mma_newsletter_save_data', 'mma_newsletter_save_data' );

// Create shortcode
function mma_newsletter_shortcode_func( $atts )
{
	ob_start();
	mma_newsletter_template();
	$output = ob_get_contents();
	ob_end_clean();
	return $output;
}

add_shortcode( 'mmanewsletter', 'mma_newsletter_shortcode_func' ); // Register shortcode hook, shortcode is [mmanewsletter]
add_filter('widget_text', 'do_shortcode'); // Enable shortcodes in widgets


// Create widget of the plugin
class MMANewsletterWidget extends WP_Widget {
	function __construct()
    {
        parent::__construct('MMA_Newsletter', 'MMA Newsletter', array( 'description' => __( 'MMA Newsletter widget', 'text_domain' ), ));
    }

	function widget( $args, $instance ) {
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );

		echo $before_widget;

	  	if ($title) {
			echo $before_title . $title . $after_title;
	  	}

		mma_newsletter_template();
	   	echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = strip_tags( $new_instance['title'] );

		return $instance;
	}

	function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		echo "<label for=".$this->get_field_id( 'title' ).">"._e( 'Title:' );
		echo "<input class='widefat' id=".$this->get_field_id( 'title' )." name=".$this->get_field_name( 'title' )." type='text' value='".$title."'>";
		echo "</label>";
	}
}

add_action( 'widgets_init', create_function( '', 'register_widget( "MMANewsletterWidget" );' ) );

// Admin hook for the menu
add_action('admin_menu', 'mma_newsletter_menu');
