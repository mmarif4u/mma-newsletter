jQuery(document).ready(function($) {

  	$(function() {
  		$( ".datepicker_from" ).datepicker();
        $( ".datepicker_from" ).change(function() {
            $( ".datepicker_from" ).datepicker( "option", "dateFormat", 'yy-mm-dd' );
        });
        
		$( ".datepicker_to" ).datepicker();
		$( ".datepicker_to" ).change(function() {
			$( ".datepicker_to" ).datepicker( "option", "dateFormat", 'yy-mm-dd' );
		});
    });

}); 
