=== MMA Newsletter ===
Contributors: Mian Muhammad Arif
Donate link: (paypal) mmarif4u@gmail.com
Tags: newsletter, email subscription
Requires at least: 3.0.1
Tested up to: 3.5
Stable tag: 0.1
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A simple newsletter plug for getting users email and exporting the data into CSV format.

== Description ==

WordPress Plugin for collecting user emails and exporting it to CSV from WP Admin area.

== Installation ==

1- Just download the plug and copy it to wp-content/plugins folder.
2- Go to WP admin area and activate it from the Plugins page.
3- It will create a new table and a new menu link, from where the data can be exported to csv selecting diff dates.
4- To use it in your themes, either use the widget OR place the shortcode in any page. (more details below in usage).
5- And done!!!

= Usage =

[Widget] Place it anywhere in your sidebars/footer.
[Shortcode] Place the shortcode anywhere in your pages. Shortcode is [mmanewsletter].
[Function] Or either place this code in your theme templates where you want to use it:
<?php
if (function_exists("mma_newsletter_template")) {
  echo mma_newsletter_template ();
}
?>

= Modifications =

If you are not happy with the template, how it look like. Just go to the plugin folder and make changes to the template.php file and css/styles.css. But be careful to not change the ids assigned to the input text box and button.

= Issues/Help =

Got any issue, https://github.com/mmarif4u/mma-newsletter/issues

== Screenshots ==

http://anl4u.com/blog/newsletter-plugin-for-wordpress/

== Changelog ==

= 0.1 =
* Simple yet configurable.
* Exporting data based on selected dates.
* Easy to configure the template.

== Upgrade Notice ==
See installation

== Frequently Asked Questions ==
Q- Will this plug have updates?
A- Yes, it will have from time to time.

Q- Will new features be added?
A- That will depend on the plug popularity, but still i would like to keep it as simple as possible.

Q- How can i get help?
A- You can create issue on github page OR ask in the support forum.
