MMA Newsletter
==============
  
Tags: newsletter, email subscription  
Requires at least: 3.0.1  
Tested up to: 3.5  
Stable tag: 0.1  
License: GPLv2  
License URI: http://www.gnu.org/licenses/gpl-2.0.html  

### Description:
WordPress Plugin for collecting user emails and exporting it to CSV from WP Admin area.  

### Installation:
1- Just download the plug and copy it to wp-content/plugins folder.  
2- Go to WP admin area and activate it from the Plugins page.  
3- It will create a new table and a new menu link, from where the data can be exported to csv selecting diff dates.  
4- To use it in your themes, either use the widget OR place the shortcode in any page. (more details below in usage).  
5- And done!!!  

### Usage:
[Widget] Place it anywhere in your site sidebar / footer.  
[Shortcode] Place the shortcode anywhere in your site pages. Shortcode is [mmanewsletter].  
[Function] Or either place this code in your theme templates where you want to use it:  

```php
<?php
    if (function_exists("mma_newsletter_template")) {
        echo mma_newsletter_template();
    }
?>
``` 

### Issues / MR:
Got any issue or need help, open issue here https://gitlab.com/mmarif4u/mma-newsletter/issues.  
Want to enhance it, open new MR.

### Screenshots
https://www.therandombits.com/914/newsletter-plugin-for-wordpress/  

### Modifications:
If you want to change the look and feel of the template. Just go to the plugin folder and make changes to the template.php file and css/styles.css. But be careful to not change the ids assigned to the input text box and button. 

### Changelog
0.1
- Simple yet configurable.
- Exporting data based on selected dates.
- Easy to configure the template.

### Upgrade Notice
See installation  
