<?php
/* Admin module */

if(isset($_POST['exporttocsv'])){

	// Function to export data into csv
	function mma_newsletter_export_data () {
		global $wpdb;
		$tbl_name = MMA_NEWSLETTER_PLUGIN_TABLE;
		$datefrom = $_POST['from'];
		$datefrom = $datefrom." 00:00:01";
		$dateto = $_POST['to'];
		$dateto = $dateto." 23:59:59";

		$csv_ = $wpdb->get_results ( $wpdb->prepare( " SELECT email, time FROM $tbl_name where time between %s and %s " , $datefrom, $dateto ));
	 	
	 	if ($csv_){

			$columns = array('EMAIL','TIME');
			$filep = fopen('php://output', 'w');
			if ($filep) {
				$file = uniqid().'_'.time().'.csv';
			    header("Content-type: application/x-msdownload");
			    header("Content-Disposition: attachment; filename=$file");
			    header("Pragma: no-cache");
			    header("Expires: 0");
			    fputcsv($filep, $columns);

			    foreach($csv_ as $data) {
			    	$data_ = array($data->email, $data->time);
			    	fputcsv($filep, $data_);
			    }
			    die;
			}

		}
	}

	add_action('admin_init', 'mma_newsletter_export_data');
	
}

//add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );

function mma_newsletter_menu() {
	add_menu_page( 'MMA Newsletter Options Page', 'MMA Newsletter', 'manage_options', 'mma_newsletter', 'mma_newsletter_options' );
}

function mma_newsletter_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	echo '<h3>MMA Newsletter Options Page</h3>';

?>
	<p><strong> Select date range </strong></p>
	<form action='' method='post'>
	<p> From : <input type="text" id="datepicker_from" name="from" class='datepicker_from' value='' /> &nbsp;&nbsp;&nbsp; To : <input type="text" id="datepicker_to" name="to" class='datepicker_to' value='' /></p>
	<p> <input type="submit" id="exporttocsv" name='exporttocsv' Value='Export to csv' /></p>	
	</form>

<?php
}

function activate() {
    $role = get_role('administrator');
    $role->add_cap('download_csv');
}
register_activation_hook( __FILE__ , 'activate');
